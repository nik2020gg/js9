function displayArrayAsList(array, parent = document.body) {
  array.forEach((item) => {
    const listItem = document.createElement("li");
    listItem.textContent = item;

    list.appendChild(listItem);
  });

  parent.appendChild(list);
}

const exampleArray1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const exampleArray2 = ["1", "2", "3", "sea", "user", 23];

displayArrayAsList(exampleArray1);
displayArrayAsList(exampleArray2);
